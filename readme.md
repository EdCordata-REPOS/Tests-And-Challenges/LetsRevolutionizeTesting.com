# Solution to LetsRevolutionizeTesting.com

```ruby
class FinishChallenge
  require 'open-uri'
  require 'json'

  def initialize(url, debug: false)
    @url          = url
    @end_url      = @url
    @end_response = nil
    @debug        = debug
  end # initialize

  def run
    perform_loop
    { url: @end_url, response: @end_response }
  end # run

  private

  def perform_loop
    while ( @end_response = get(@end_url) ).has_key?('follow')
      if @debug
        puts @end_url
        puts @end_response
        puts ''
      end

      @end_url = @end_response['follow']
    end
  end # perform_loop

  def get(url)
    ::JSON.parse( ::URI.open( url, 'Accept' => 'application/json' ).string )
  end # get

end # FinishChallenge

puts ::FinishChallenge.new( 'https://letsrevolutionizetesting.com/challenge', debug: true ).run
```
